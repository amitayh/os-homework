#include "types.h"
#include "fcntl.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]) {
  char* path = argv[1];
  int n = atoi(argv[2]);
  struct stat stat;
  char buf[512];
  int fd;
  
  if ((fd = open(path, O_RDONLY)) < 0 || fstat(fd, &stat) < 0) {
    printf(2, "filesize: cannot open %s\n", path);
    exit();
  }

  lseek(fd, stat.size - n, '\0');
  while ((n = read(fd, buf, sizeof(buf))) > 0) {
    write(1, buf, n);
  }
  
  close(fd);
  exit();
}
