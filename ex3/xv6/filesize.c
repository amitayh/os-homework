#include "types.h"
#include "fcntl.h"
#include "stat.h"
#include "user.h"

int main(int argc, char *argv[]) {
  char* path = argv[1];
  struct stat stat;
  int fd;
  if ((fd = open(path, O_RDONLY)) < 0 || fstat(fd, &stat) < 0) {
    printf(2, "filesize: cannot open %s\n", path);
    exit();
  }
  printf(1, "size of file f1 is: %d\n", stat.size);
  close(fd);
  exit();
}
