#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>

#define N               4
#define RAND_MAX_VALUE  100
#define RAND_SLEEP      5
#define PIPE_READ       0
#define PIPE_WRITE      1

////////////////////////////////////////////////////////////////////////////////
// Prototypes & typedefs
////////////////////////////////////////////////////////////////////////////////

int redirect_stderr(char*);
void restore_stderr(int);
void fatal_error(char*);
void new_child_process_created(int);
void open_pipes(int[], int[]);
void create_writers(int, int[]);
void create_writer(int[]);
void create_reader(int[], int[]);
void seed_random();
int get_random(int);
void do_child_writer(int[]);
void do_child_reader(int[], int[]);
void random_sleep();
void wait_for_all_children(int);
void close_all(int[]);

struct data {
    int pid;
    int num;
};

////////////////////////////////////////////////////////////////////////////////
// Implementation
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[]) {
    int pid = getpid();
    int children_pipe[2];
    int total_pipe[2];
    int original_stderr;
    int sum;

    open_pipes(children_pipe, total_pipe);

    original_stderr = redirect_stderr("children.err");
    create_writers(N, children_pipe);
    create_reader(children_pipe, total_pipe);
    restore_stderr(original_stderr);
    close_all(children_pipe);

    read(total_pipe[PIPE_READ], &sum, sizeof(sum));
    fprintf(stdout, "(%d) Parent - total sum is: %d\n", pid, sum);
    close_all(total_pipe);

    wait_for_all_children(N + 1);
    fprintf(stderr, "(%d) all my child processes are done, I'm going to exit!\n", pid);
    
    return EXIT_SUCCESS;
}

int redirect_stderr(char* filename) {
    int original_stderr = dup(STDERR_FILENO);
    close(STDERR_FILENO);
    if (open(filename, O_WRONLY | O_CREAT | O_TRUNC, S_IRWXU | S_IRGRP | S_IROTH) < 0) {
        fatal_error("Cannot open file");
    }
    return original_stderr;
}

void restore_stderr(int original_stderr) {
    dup2(original_stderr, STDERR_FILENO);
}

void open_pipes(int children_pipe[], int total_pipe[]) {
    if (pipe(children_pipe) == -1 || pipe(total_pipe) == -1) {
        close_all(children_pipe);
        close_all(total_pipe);
        fatal_error("Cannot open pipe");
    }
}

void fatal_error(char* message) {
    fprintf(stderr, "ERROR: %s\n", message);
    exit(EXIT_FAILURE);
}

void create_writers(int number, int fd[]) {
    int i;
    for (i = 0; i < number; i++) {
        create_writer(fd);
    }
}

void create_writer(int fd[]) {
    int pid = fork();
    if (pid < 0) {
        // Handle error
        close_all(fd);
        fatal_error("Cannot fork");
    } else if (pid == 0) {
        // Handle child
        do_child_writer(fd);
        exit(EXIT_SUCCESS);
    } else {
        // Handle parent
        new_child_process_created(pid);
    }
}

void create_reader(int children_pipe[], int total_pipe[]) {
    int pid = fork();
    if (pid < 0) {
        // Handle error
        close_all(children_pipe);
        close_all(total_pipe);
        fatal_error("Cannot fork");
    } else if (pid == 0) {
        // Handle child
        do_child_reader(children_pipe, total_pipe);
        exit(EXIT_SUCCESS);
    } else {
        // Handle parent
        new_child_process_created(pid);
    }
}

void seed_random() {
    // Seed random number generator using PID
    int pid = getpid();
    srand(pid);
}

int get_random(int max) {
    return ((int) random() % max) + 1;
}

void new_child_process_created(int child_pid) {
    printf("(%d) just created a process %d\n", getpid(), child_pid);
}

void do_child_writer(int fd[]) {
    struct data my_data;

    close(fd[PIPE_READ]);

    seed_random();
    random_sleep();

    my_data.pid = getpid();
    my_data.num = get_random(RAND_MAX_VALUE);

    fprintf(stderr, "(%d) my number is: %d\n", my_data.pid, my_data.num);

    write(fd[PIPE_WRITE], &my_data, sizeof(my_data));

    close(fd[PIPE_WRITE]);
}

void do_child_reader(int children_pipe[], int total_pipe[]) {
    int sum = 0;
    int pid = getpid();
    struct data read_data;

    close(children_pipe[PIPE_WRITE]);
    close(total_pipe[PIPE_READ]);

    seed_random();
    random_sleep();
    fprintf(stderr, "(%d) hello from reader!\n", pid);

    while (read(children_pipe[PIPE_READ], &read_data, sizeof(read_data)) > 0) {
        fprintf(stdout, "(%d) process %d got number: %d\n", pid, read_data.pid, read_data.num);
        sum += read_data.num;
    }

    fprintf(stdout, "(%d) child-reader – Total is %d\n", pid, sum);
    write(total_pipe[PIPE_WRITE], &sum, sizeof(sum));

    close(children_pipe[PIPE_READ]);
    close(total_pipe[PIPE_WRITE]);
}

void random_sleep() {
    int sleep_time = get_random(RAND_SLEEP);
    sleep(sleep_time);
    fprintf(stderr, "(%d) back from sleep of %d seconds\n", getpid(), sleep_time);
}

void wait_for_all_children(int num_children) {
    int status, i;
    for (i = 0; i < num_children; i++) {
        wait(&status);
    }
}

void close_all(int fd[]) {
    close(fd[PIPE_READ]);
    close(fd[PIPE_WRITE]);
}
