#include "types.h"
#include "stat.h"
#include "user.h"

#define OUT 1
#define ERR 2

void fatal_error(char* message) {
    printf(ERR, "Error: %s\n", message);
    exit();
}

void print_proc_data(int pid) {
    struct proc_data data;

    if (pid == 0) {
        return;
    }
    if (get_proc_data(pid, &data) != 0) {
        fatal_error("Cannot get porc data");
    }

    printf(OUT, "%d %s %d\n", pid, data.name, data.size);
    print_proc_data(data.ppid);
}

void print_proc_data_recursive(int depth) {
    int pid;

    if (depth == 0) {
        print_proc_data(getpid());
        return;
    }

    pid = fork();
    if (pid < 0) {
        // Handle error
        fatal_error("Cannot fork");
    } else if (pid == 0) {
        // Handle child
        print_proc_data_recursive(depth - 1);
        exit();
    } else {
        // Handle parent
        wait();
    }
}

int main(int argc, char *argv[]) {
    int depth;

    if (argc != 2) {
        fatal_error("Invalid number of arguments");
    }

    depth = atoi(argv[1]);
    printf(OUT, "PID NAME SIZE\n");
    print_proc_data_recursive(depth);

    exit();
}