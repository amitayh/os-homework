#include "types.h"
#include "stat.h"
#include "user.h"

#define COUNT 10
#define ARR_SIZE 256

  struct dummy {
	int arr[ARR_SIZE];
	int j;
	} dum_arr[COUNT];

int
main(int argc, char *argv[])
{
  
  int i;
  for(i = 1; i < COUNT; i++)
    v2p_print(&dum_arr[i]);
	
  // printf(1, "trying dummy addresses: first NULL and then 0x12345678...\n");
  // v2p_print((void*)0);
  // v2p_print((void*)0x12345678);
  
  exit();
}
