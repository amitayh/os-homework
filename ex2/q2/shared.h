#ifndef SHARED_H
#define SHARED_H

#include "semaphore.h"

#define BUF_SIZE           10   // free slots or spaces in each cyclic buffer
#define N_WORKERS           4   // number of worker processes created by parent
#define MAX_RAND           20   // for creating random numbers from 1 to K
#define ENOUGH              8   // stop after this number of pitagorian triples
#define U_SLEEP         10000   // micro-sec of delay
#define MEM_NAME  "mem_pitag"   // shared memory name

#define SEM_SHM_READY_NAME          "/sem_shm_ready"
#define SEM_SHM_READY_INIT          0
#define SEM_BUFFER_RW_NAME          "/sem_buffer_rw"
#define SEM_BUFFER_RW_INIT          1
#define SEM_ITEM_AVAILABLE_NAME     "/sem_item_available"
#define SEM_ITEM_AVAILABLE_INIT     0
#define SEM_ITEM_NEEDED_NAME        "/sem_item_needed"
#define SEM_ITEM_NEEDED_INIT        BUF_SIZE
#define SEM_HAS_ENOUGH_NAME         "/sem_has_enough"
#define SEM_HAS_ENOUGH_INIT         0

struct cyclic_buf;

struct sem_context {
  sem_t* shm_ready;
  sem_t* buffer_rw;
  sem_t* item_available;
  sem_t* item_needed;
  sem_t* has_enough;
};

void open_sem_context(struct sem_context*);
void close_sem_context(struct sem_context*);
sem_t* sem_open_safe(char*, int);
struct cyclic_buf* open_shared_memory(int*, int);
void fatal_error(char*);

#endif
