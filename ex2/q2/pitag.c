#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

#include "shared.h"
#include "cyclic_buf.h"

///////////////////////////////////////////////////////////////////////////////
// Prototypes
///////////////////////////////////////////////////////////////////////////////

struct triple {
  int a, b, c;
};

void read_triple(struct sem_context*, struct cyclic_buf*, struct triple*);
int read_item(struct sem_context*, struct cyclic_buf*);
int is_pitagorian(struct triple*);
void print_pitagorian(struct triple*, int);
void has_enough(struct sem_context*, int);
void close_shared_memory();

///////////////////////////////////////////////////////////////////////////////
// Main
///////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[]) {
  struct sem_context sem_context;
  struct triple triple;
  struct cyclic_buf* buf;
  int found, fd;

  open_sem_context(&sem_context);
  buf = open_shared_memory(&fd, 1);
  sem_post(sem_context.shm_ready);

  found = 0;
  while (found < ENOUGH) {
    read_triple(&sem_context, buf, &triple);
    if (is_pitagorian(&triple)) {
      print_pitagorian(&triple, ++found);
    }
  }

  has_enough(&sem_context, ENOUGH);
  close_sem_context(&sem_context);
  close(fd);

  return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
// Implementations
///////////////////////////////////////////////////////////////////////////////

void read_triple(struct sem_context* sem_context, struct cyclic_buf* buf, struct triple* triple) {
  triple->a = read_item(sem_context, buf);
  triple->b = read_item(sem_context, buf);
  triple->c = read_item(sem_context, buf);
}

int read_item(struct sem_context* sem_context, struct cyclic_buf* buf) {
  int item;
  sem_wait(sem_context->item_available);
  sem_wait(sem_context->buffer_rw);
  if (!cyclic_buf_take(buf, &item)) {
    fatal_error("Unable to read from buffer");
  }
  fprintf(stderr, "(%d) removed %d from buffer\n", getpid(), item);
  sem_post(sem_context->buffer_rw);
  sem_post(sem_context->item_needed);
  return item;
}

int is_pitagorian(struct triple* triple) {
  int a = triple->a,
      b = triple->b,
      c = triple->c,
      a2 = a * a,
      b2 = b * b,
      c2 = c * c;

  return a2 + b2 == c2 ||
         a2 + c2 == b2 ||
         b2 + c2 == a2;
}

void print_pitagorian(struct triple* triple, int total_found) {
  printf("found pitagorian triple: %d %d %d (success so far: %d)\n",
    triple->a, triple->b, triple->c, total_found);
}

void has_enough(struct sem_context* sem_context, int enough) {
  printf("ENOUGH (%d) going to TERMINATE\n", enough);
  sem_post(sem_context->has_enough);
}
