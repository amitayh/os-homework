How to run:
===========
I've prepared a Makefile to make things easier.

1. make - compile the worker and pitag programs
2. make test - test the cyclic buffer implementation
3. meke clean - removes compiled target and cleans shared memory / semaphores

Please run `make clean && make` before executing the programs!
Also, the worker should be run first, then pitag (altough it should also work
the other way around)

Synchronization:
================
I used 5 semaphores, that are grouped in a sem_context struct (shared.h):

1.  shm_ready - indicates that shared memory is ready (workers wait for
    pitag to signal after initialization)
2.  buffer_rw - mutex for read/write to cyclic buffer (wait before
    read/write, signal after). Init value is 1 - allow someone to write
3.  item_available - workers signal when they put items in the buffer.
    pitag waits before reading the values
4.  item_needed - pitag signals after reading a value. worker waits before
    writing. Init value is BUF_SIZE
5.  has_enough - indicates the enough triplets were found. pitag signals when
    exiting the main loop. Workers in turn check this value using trywait
    (don't block if more values need to be produces). Once a worker aquires
    this semaphore, it immediately signals again to allow other workers to
    finish as well