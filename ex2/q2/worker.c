#include "stdlib.h"
#include "stdio.h"
#include "unistd.h"
#include "sys/wait.h"

#include "shared.h"
#include "cyclic_buf.h"

///////////////////////////////////////////////////////////////////////////////
// Prototypes
///////////////////////////////////////////////////////////////////////////////

void start_workers(int);
void start_worker();
int needs_more(struct sem_context*);
void write_item(struct sem_context*, struct cyclic_buf*);
int get_number();
void wait_for_children(int);

///////////////////////////////////////////////////////////////////////////////
// Main
///////////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[]) {
  struct sem_context sem_context;

  open_sem_context(&sem_context);
  printf("waiting for shared memory...\n");
  sem_wait(sem_context.shm_ready);
  printf("shared memory ready\n");
  start_workers(N_WORKERS);
  wait_for_children(N_WORKERS);
  close_sem_context(&sem_context);

  printf("(%d) main process going to exit (all child processes terminated)\n", getpid());
  return EXIT_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////
// Implementations
///////////////////////////////////////////////////////////////////////////////

void start_workers(int num_workers) {
  int i, pid;
  for (i = 0; i < num_workers; i++) {
    pid = fork();
    if (pid < 0) {
      fatal_error("Unable to fork");
    } else if (pid == 0) {
      printf("worker %d starting\n", i);
      start_worker();
      printf("worker %d finished\n", i);
      exit(EXIT_SUCCESS);
    }
  }
}

void start_worker() {
  struct sem_context sem_context;
  struct cyclic_buf* buf;
  int fd;

  srand(getpid());
  open_sem_context(&sem_context);
  buf = open_shared_memory(&fd, 0);

  while (needs_more(&sem_context)) {
    write_item(&sem_context, buf);
  }

  close(fd);
  close_sem_context(&sem_context);
}

int needs_more(struct sem_context* sem_context) {
  if (sem_trywait(sem_context->has_enough) < 0) {
    // semaphore was not signaled yet
    return 1;
  } else {
    // semaphore was signaled - allow others to read signal
    sem_post(sem_context->has_enough);
    sem_post(sem_context->item_needed);
    return 0;
  }
}

void write_item(struct sem_context* sem_context, struct cyclic_buf* buf) {
  int item = get_number();
  sem_wait(sem_context->item_needed);
  sem_wait(sem_context->buffer_rw);
  cyclic_buf_put(buf, item);
  fprintf(stderr, "(%d) wrote %d to buffer\n", getpid(), item);
  sem_post(sem_context->buffer_rw);
  sem_post(sem_context->item_available);
}

int get_number() {
  return (rand() % MAX_RAND) + 1;
}

void wait_for_children(int num_children) {
  int i, status;
  for (i = 0; i < num_children; i++) {
    wait(&status);
  }
}
