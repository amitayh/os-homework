#include "assert.h"
#include "stdlib.h"
#include "stdio.h"

#include "cyclic_buf.h"

int main(int argc, char* argv[]) {
  struct cyclic_buf buf;
  int i, n;

    // Init
  cyclic_buf_init(&buf);
  assert(!cyclic_buf_is_full(&buf));

    // Put
  for (i = 0; i < BUF_SIZE; i++) {
    assert(cyclic_buf_put(&buf, i));
  }

    // Buffer full
  assert(!cyclic_buf_put(&buf, 1));
  assert(cyclic_buf_is_full(&buf));

    // Take
  for (i = 0; i < BUF_SIZE; i++) {
    assert(cyclic_buf_take(&buf, &n));
    assert(!cyclic_buf_is_full(&buf));
    assert(n == i);
  }
  assert(!cyclic_buf_take(&buf, &n));

  printf("OK\n");
  return EXIT_SUCCESS;
}