#include "unistd.h"

#include "cyclic_buf.h"

void cyclic_buf_init(struct cyclic_buf* buf) {
  buf->nread = 0;
  buf->nwrite = 0;
}

int cyclic_buf_is_full(struct cyclic_buf* buf) {
  return (buf->nwrite - buf->nread) == BUF_SIZE;
}

int cyclic_buf_put(struct cyclic_buf* buf, int data) {
  usleep(U_SLEEP);
  if (cyclic_buf_is_full(buf)) {
    return 0;
  }
  buf->data[buf->nwrite % BUF_SIZE] = data;
  buf->nwrite++;
  return 1;
}

int cyclic_buf_take(struct cyclic_buf* buf, int* out) {
  usleep(U_SLEEP);
  if (buf->nread == buf->nwrite) {
    return 0;
  }
  *out = buf->data[buf->nread % BUF_SIZE];
  buf->nread++;
  return 1;
}
