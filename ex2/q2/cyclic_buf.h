#include "shared.h"

struct cyclic_buf {
  int data[BUF_SIZE];
  int nread;
  int nwrite;
};

void cyclic_buf_init(struct cyclic_buf*);
int cyclic_buf_is_full(struct cyclic_buf*);
int cyclic_buf_put(struct cyclic_buf*, int);
int cyclic_buf_take(struct cyclic_buf*, int*);
