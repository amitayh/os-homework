#include "stdio.h"
#include "stdlib.h"
#include "fcntl.h"
#include "unistd.h"
#include "sys/mman.h"

#include "shared.h"
#include "cyclic_buf.h"

void open_sem_context(struct sem_context* sem_context) {
  sem_context->shm_ready = sem_open_safe(SEM_SHM_READY_NAME, SEM_SHM_READY_INIT);
  sem_context->buffer_rw = sem_open_safe(SEM_BUFFER_RW_NAME, SEM_BUFFER_RW_INIT);
  sem_context->item_available = sem_open_safe(SEM_ITEM_AVAILABLE_NAME, SEM_ITEM_AVAILABLE_INIT);
  sem_context->item_needed = sem_open_safe(SEM_ITEM_NEEDED_NAME, SEM_ITEM_NEEDED_INIT);
  sem_context->has_enough = sem_open_safe(SEM_HAS_ENOUGH_NAME, SEM_HAS_ENOUGH_INIT);
}

void close_sem_context(struct sem_context* sem_context) {
  sem_close(sem_context->shm_ready);
  sem_close(sem_context->buffer_rw);
  sem_close(sem_context->item_available);
  sem_close(sem_context->item_needed);
  sem_close(sem_context->has_enough);
}

sem_t* sem_open_safe(char* name, int init) {
  sem_t* sem = sem_open(name, O_CREAT, S_IRWXU, init);
  if (sem == SEM_FAILED) {
    fatal_error("Unable to open semaphore");
  }
  return sem;
}

struct cyclic_buf* open_shared_memory(int* pfd, int truncate) {
  int fd;
  struct cyclic_buf* ptr;

  // Open shared memory
  fd = shm_open(MEM_NAME, O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
  if (fd == -1) {
    fatal_error("Unable to open shared memory");
  }

  // Truncate if needed
  if (truncate && ftruncate(fd, sizeof(struct cyclic_buf)) == -1) {
    close(fd);
    fatal_error("Unable to truncate shared memory");
  }

  // Map shared memory
  ptr = (struct cyclic_buf*) mmap(NULL, sizeof(struct cyclic_buf), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if (ptr == MAP_FAILED) {
    close(fd);
    fatal_error("Unable to map shared memory");
  }

  // Update file descriptor
  *pfd = fd;

  return ptr;
}

void fatal_error(char* message) {
  fprintf(stderr, "Error: %s\n", message);
  exit(EXIT_FAILURE);
}