#include "types.h"
#include "stat.h"
#include "user.h"

#define STDOUT 1
#define STDERR 2
#define NUM_CHILDREN 6
#define SEM_NAME "mysem"
#define SEM_INIT 2
#define SEM_MAX 3

void fatal_error(char*);
int open_semaphore();
void create_chilren(int, int);
void create_child(int, int);
void do_child_work(int, int);
void wait_for_children(int);

int main(int argc, char* argv[]) {
  int sd;
  printf(STDOUT, "(PARENT) Starting...\n");
  sd = open_semaphore();
  create_chilren(sd, NUM_CHILDREN);
  wait_for_children(NUM_CHILDREN);
  sem_close(sd);
  printf(STDOUT, "(PARENT) Done\n");
  exit();
}

void fatal_error(char* message) {
  printf(STDERR, "Error: %s\n", message);
  exit();
}

int open_semaphore() {
  int sd;
  if ((sd = sem_open(SEM_NAME, 1, SEM_INIT, SEM_MAX)) < 0) {
    fatal_error("Unable to open semaphore");
  }
  return sd;
}

void create_chilren(int sd, int num_children) {
  int i;
  for (i = 0; i < NUM_CHILDREN; i++) {
    create_child(sd, i);
  }
}

void create_child(int sd, int child_num) {
  int pid = fork();
  if (pid < 0) {
    fatal_error("Unable to fork");
  } else if (pid == 0) {
    printf(STDOUT, "(PARENT) Created child #%d\n", child_num);
    do_child_work(sd, child_num);
    sem_close(sd);
    exit();
  }
}

void do_child_work(int sd, int child_num) {
  printf(STDOUT, "(CHILD #%d) Waiting to enter critical section...\n", child_num);
  sem_wait(sd);
  printf(STDOUT, "(CHILD #%d) Entered critical section, doing some work...\n", child_num);
  sleep(100);
  printf(STDOUT, "(CHILD #%d) Finished work, exiting critical section...\n", child_num);
  sem_signal(sd);
}

void wait_for_children(int num_children) {
  int i;
  for (i = 0; i < num_children; i++) {
    wait();
  }
}
 