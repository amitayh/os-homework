#include "types.h"
#include "defs.h"
#include "param.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"

#define SEM_NAME_LENGTH 6

struct sem {
  struct spinlock sslock; // single semaphore lock
  char name[SEM_NAME_LENGTH + 1];
  int ref; // reference count
  int val;
  int max;
};

struct {
  struct spinlock gslock; //global semaphore lock
  struct sem sem[NSEM];
} stable;

// Allocate a semaphore structure.
static struct sem* semalloc(void) {
  struct sem *sem;

  acquire(&stable.gslock);
  for (sem = stable.sem; sem < stable.sem + NSEM; sem++) {
    if (sem->ref == 0) {
      sem->ref = 1;
      release(&stable.gslock);
      return sem;
    }
  }
  release(&stable.gslock);
  return 0;
}

// Allocate a semaphore descriptor for the given semaphore.
// Takes over semaphore reference from caller on success.
static int sdalloc(struct sem *sem) {
  int sd;

  for (sd = 0; sd < NOSEM; sd++) {
    if (proc->osem[sd] == 0) {
      proc->osem[sd] = sem;
      return sd;
    }
  }
  return -1;
}

// Find a semaphore with given name.
// Return NULL if not found
static struct sem* semfind(char* name) {
  struct sem *sem;

  acquire(&stable.gslock);
  for (sem = stable.sem; sem < stable.sem + NSEM; sem++) {
    if (sem->ref > 0 && strncmp(sem->name, name, SEM_NAME_LENGTH) == 0) {
      release(&stable.gslock);
      return sem;
    }
  }
  release(&stable.gslock);
  return 0;
}

// Init sem struct
void sem_init(struct sem* sem, char* name, int val, int max) {
  strncpy(sem->name, name, SEM_NAME_LENGTH);
  sem->val = val;
  sem->max = max;
}

int sem_open(char* name, int create, int init, int max) {
  struct sem* sem;
  int sd;

  sem = semfind(name);
  if (sem == 0) {
    if (!create) {
      // Couldn't find semaphore and create is false
      return -1;
    }
    if ((sem = semalloc()) == 0) {
      // Unable to allocate semaphore
      return -1;
    }
    sem_init(sem, name, init, max);
  } else {
    // Increment reference count
    sem->ref++;
  }

  if ((sd = sdalloc(sem)) < 0) {
    // Unable to allocate semaphore descriptor,
    // close allocated semaphore
    sem_close(sem);
    return -1;
  }

  return sd;
}

void sem_close(struct sem* sem) {
  acquire(&stable.gslock);
  if (sem->ref < 1) {
    panic("sem_close");
  }
  // Decrement reference count (effectively frees the semaphore)
  sem->ref--;
  release(&stable.gslock);
}

struct sem* sem_dup(struct sem* sem) {
  acquire(&stable.gslock);
  if (sem->ref < 1) {
    panic("sem_dup");
  }
  // Inccrement reference count
  sem->ref++;
  release(&stable.gslock);
  return sem;
}

int sem_wait(struct sem* sem) {
  acquire(&sem->sslock);
  while (sem->val == 0) {
    if (proc->killed) {
      release(&sem->sslock);
      return -1;
    }
    sleep(sem, &sem->sslock);
  }
  sem->val--;
  release(&sem->sslock);
  return 0;
}

int sem_trywait(struct sem* sem) {
  acquire(&sem->sslock);
  if (sem->val == 0) {
    release(&sem->sslock);
    return -1;
  }
  sem->val--;
  release(&sem->sslock);
  return 0;
}

int sem_signal(struct sem* sem) {
  acquire(&sem->sslock);
  if (sem->val == sem->max) {
    // Semaphore already at max value
    release(&sem->sslock);
    return -1;
  }
  sem->val++;
  wakeup(sem);
  release(&sem->sslock);
  return 0;
}
