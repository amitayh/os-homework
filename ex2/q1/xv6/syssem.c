#include "types.h"
#include "defs.h"
#include "param.h"
#include "mmu.h"
#include "proc.h"

// Fetch the nth word-sized system call argument as a semaphore descriptor
// and return both the descriptor and the corresponding struct sem.
static int argsd(int n, int *psd, struct sem **psem) {
  int sd;
  struct sem *sem;
  if (argint(n, &sd) < 0) {
    return -1;
  }
  if (sd < 0 || sd >= NOSEM || (sem = proc->osem[sd]) == 0) {
    return -1;
  }
  if (psd) {
    *psd = sd;
  }
  if (psem) {
    *psem = sem;
  }
  return 0;
}

int sys_sem_open(void) {
  char *name;
  int create;
  int init;
  int maxVal;
  if (argstr(0, &name) < 0 ||
      argint(1, &create) < 0 ||
      argint(2, &init) < 0 ||
      argint(3, &maxVal) < 0) {
    return -1;
  }
  return sem_open(name, create, init, maxVal);
}

int sys_sem_close(void) {
  int sd;
  struct sem *sem;
  if (argsd(0, &sd, &sem) < 0) {
    return -1;
  }
  proc->osem[sd] = 0;
  sem_close(sem);
  return 0;
}

int sys_sem_wait(void) {
  struct sem *sem;
  if (argsd(0, 0, &sem) < 0) {
    return -1;
  }
  return sem_wait(sem);
}

int sys_sem_trywait(void) {
  struct sem *sem;
  if (argsd(0, 0, &sem) < 0) {
    return -1;
  }
  return sem_trywait(sem);
}

int sys_sem_signal(void) {
  struct sem *sem;
  if (argsd(0, 0, &sem) < 0) {
    return -1;
  }
  return sem_signal(sem);
}
