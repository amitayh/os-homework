Semaphore implementation:
=========================
syssem.c: All semaphore related system calls reach this file. sys_sem_* simply
extract arguments from user stack, and call sem_* functions (which are
implemented in sem.c)

sem.c: Semaphore implementaion is similar to file handling - a global stable
holds sem structs. When a process opens a semaphre, a new entry in
process->osem is used, giving the user program access via semaphore descriptor.
The sem_wait/trywait and sem_signal functions use the spinlock on the semaphore
struct to control the access.

User program:
=============
The main process forks and creates 6 child processes. Each child process tries
to enter a critical section and do some work (for the demonstration - just
sleep for 100 ticks). The critical section is protected by a semaphore named
"mysem", with initial value of 2. The children then call sem_wait() to enter
the critical section, and call sem_signal() when they are finished - to
indicate the other processes.
